from dynamixel_sdk import *

portHandler = None
packetHandler = None


def init(dev, baud):
    PROTOCOL_VERSION = 1.0
    global portHandler, packetHandler
    portHandler = PortHandler(dev)
    packetHandler = PacketHandler(PROTOCOL_VERSION)
    if not portHandler.openPort():
        print('Failed to openPort')
        return False
    if not portHandler.setBaudRate(baud):
        print('Failed to setBaudRate')
        return False
    return True


# Control table address
# https://emanual.robotis.com/docs/en/dxl/mx/mx-64/#initial-value
ADDR_MX_TORQUE_ENABLE = 24
ADDR_MX_GOAL_POSITION = 30
ADDR_MX_PROFILE_VELOCITY = 32
ADDR_MX_PRESENT_POSITION = 36
ADDR_MX_CW_ANGLE_LIMIT = 6
ADDR_MX_CCW_ANGLE_LIMIT = 8


class Dynamixel:
    def __init__(self, idd):
        self.idd = idd

    def _write(self, addr, val, is_2Byte=True):
        dxl_comm_result, dxl_error = 0, 0
        if is_2Byte:
            dxl_comm_result, dxl_error = packetHandler.write2ByteTxRx(
                portHandler, self.idd, addr, val)
        else:
            dxl_comm_result, dxl_error = packetHandler.write1ByteTxRx(
                portHandler, self.idd, addr, val)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % packetHandler.getTxRxResult(dxl_comm_result))
            return False
        elif dxl_error != 0:
            print("%s" % packetHandler.getRxPacketError(dxl_error))
            return False
        return True

    def _read2B(self, addr):
        global packetHandler, portHandler
        val, dxl_comm_result, dxl_error = packetHandler.read2ByteTxRx(
            portHandler, self.idd, addr)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % packetHandler.getTxRxResult(dxl_comm_result))
            return None
        elif dxl_error != 0:
            print("%s" % packetHandler.getRxPacketError(dxl_error))
            return None
        return val

    def ping(self):
        dxl_model_number, dxl_comm_result, dxl_error = packetHandler.ping(
            portHandler, self.idd)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % packetHandler.getTxRxResult(dxl_comm_result))
            return False
        elif dxl_error != 0:
            print("%s" % packetHandler.getRxPacketError(dxl_error))
            return False
        return True

    def power(self, on=True):
        return self._write(ADDR_MX_TORQUE_ENABLE, 1 if on else 0, is_2Byte=False)

    def goal(self, val=None):
        if val:
            return self._write(ADDR_MX_GOAL_POSITION, val)
        return self._read2B(ADDR_MX_GOAL_POSITION)

    def max_speed(self, rpm=None):
        if rpm:
            return self._write(ADDR_MX_PROFILE_VELOCITY, int(rpm/0.114))
        val = self._read2B(ADDR_MX_PROFILE_VELOCITY)
        if val != None:
            return int(val*0.114)
        return None

    def pos(self):
        return self._read2B(ADDR_MX_PRESENT_POSITION)

    def angle_limit(self, cw, ccw):
        return self._write(ADDR_MX_CW_ANGLE_LIMIT, cw) and self._write(ADDR_MX_CCW_ANGLE_LIMIT, ccw)


if __name__ == '__main__':
    init('/dev/ttyUSB0', 115200)
    d = Dynamixel(3)
    d.ping()
    d.power()
    d.max_speed(4)
    print(d.max_speed())
    g = d.goal()
    while True:
        import time
        d.goal(g+400)
        time.sleep(2)
        print(d.pos())
        d.goal(g)
        time.sleep(2)
        print(d.pos())
