import time
import serial

STEPPER_SET_TARGET_POS = 0
STEPPER_GET_CURRENT_POS = 1
STEPPER_SET_STEP_DELAY_MICRO_SECS = 2
STEPPER_START_HOMING = 3
STEPPER_GET_HOMING_STATUS = 4


class CNCShieldSerial:
    def __init__(self, port='/dev/ttyACM0', baudrate=115200):
        self._arduino = serial.Serial(port, baudrate, timeout=.1)
        while True:
            data = self._arduino.readline()
            if data.decode("utf-8") == 'START\r\n':
                return

    def _arduino_write(self, msg):
        self._arduino.write(msg.encode('utf-8'))
        self._arduino.flush()

    def _wait_for_msg(self):
        start = time.time()
        while start + 5 > time.time():
            data = self._arduino.readline()
            if not data:
                continue
            print(data)
            if data[0] == ord('R'):
                return data[1:].decode('utf-8').strip()
        return None

    def set_target_pos(self, stepper, pos):
        self._arduino_write('%x%x' % (STEPPER_SET_TARGET_POS, stepper))
        self._arduino_write('%04x' % pos)
        time.sleep(0.05)
        return self._wait_for_msg() == ''

    def set_step_delay(self, stepper, delay):
        self._arduino_write('%x%x' %
                            (STEPPER_SET_STEP_DELAY_MICRO_SECS, stepper))
        self._arduino_write('%04x' % delay)
        time.sleep(0.05)
        return self._wait_for_msg() == ''

    def current_pos(self, stepper):
        self._arduino_write('%x%x' % (STEPPER_GET_CURRENT_POS, stepper))
        time.sleep(0.05)
        return int(self._wait_for_msg(), base=16)

    def _get_homing_status(self, stepper):
        self._arduino_write('%x%x' %
                            (STEPPER_GET_HOMING_STATUS, stepper))
        time.sleep(0.05)
        data = self._wait_for_msg()
        if len(data) != 4:
            return None
        return int(data[:2], base=16), int(data[2:], base=16)

    def do_homing(self, stepper, direction, slowness=2):
        print('Initiate homing for ', stepper)
        self._arduino_write('%x%x' % (STEPPER_START_HOMING, stepper))
        self._arduino_write('%02x' % 1 if direction else 0)
        self._arduino_write('%02x' % slowness & 0xFF)
        while True:
            homing_stepper = self._get_homing_status()
            if homing_stepper != None:
                print('Homing in progress for ', homing_stepper)
                time.sleep(1)


def write_read(x):
    arduino.write(bytes(x, 'utf-8'))
    time.sleep(0.05)
    data = arduino.readline()
    return data


if __name__ == "__main__":
    s = CNCShieldSerial()
    print(s.current_pos(1))
    # print(s.current_pos(1))
    # print(s.current_pos(2))
    while True:
        for servo in (0, 1, 2):
            for pos in (0, 20):
                print(s.set_target_pos(servo, pos))
                time.sleep(3)
                print(s.current_pos(servo))
                print('')
