#include <assert.h>
#include <Wire.h>
#include "src/common.h"
#include "src/serial_util.h"

const byte SLAVE_ADDRESS = 0x04;


void setup() {
  setupBoard();
  Wire.begin(SLAVE_ADDRESS);
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);
  Serial.begin(115200);
  Serial.println("START");
}

void loop() {
  delay(100);
  serial_util_loop();
  for(byte i=0; i<num_steppers(); i++) {
    if (steppers[i].pos_target_offset != 0) {
      runStepper(&steppers[i]);
      return;
    }
  }
  if (homing_info.is_pending) {
    tryHoming();
  }
}

void requestEvent() {
   Serial.println("requestEvent");
   byte buf[MAX_REC_BYTES] = {0xFF, 0xFF};
   byte res_len =handleRequest(Wire.read(), buf, sizeof(buf));
   if (res_len > 0) {
     Wire.write(buf, res_len);
   }
}

void receiveEvent(int bytesReceived) {
  Serial.println("receiveEvent");
  byte buf[MAX_REC_BYTES];
  for (byte i = 0; i < bytesReceived; i++) {
    if (i < MAX_REC_BYTES) {
      buf[i] = Wire.read();
    } else {
      Wire.read();
    }
  }
  handleMsgReceive(buf, bytesReceived);
}
