#include "common.h"

byte from_hex(byte a) {
  if (a>='0' && a<='9')
    return a-'0';
  if (a>='a' && a<='f')
    return a-'a'+10;
  if (a>='A' && a<='F')
    return a-'A'+10;
  return 0xFF;
}

void serial_util_loop() {
  delay(100);
  if (Serial.available() <= 0)
    return;
  Serial.println("serial_util_loop");
  byte read[2*MAX_REC_BYTES], buf[MAX_REC_BYTES];
  byte l = Serial.readBytesUntil('\n', read, sizeof(read));
  if (l%2!=0) return;
  for(byte i=0; i<l/2; i++) {
    byte a = from_hex(read[2*i]);
    byte b = from_hex(read[2*i+1]);
    if (a==0xFF || b==0xFF) return;
    buf[i] = b+(a<<4);
  }

  if (handleMsgReceive(buf, sizeof(buf))) {
      Serial.println("R");
      Serial.flush();
      return;
  }
  byte res = handleRequest(buf[0], buf, sizeof(buf));
  if (res>0) {
    Serial.print("R");
    for(byte i=0; i<res;i++) {
      char hex[2];
      sprintf(hex, "%02x", buf[i]);
      Serial.write(hex);
    }
    Serial.print("\n");
    Serial.flush();
  }
}
