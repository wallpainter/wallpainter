#ifndef CNC_SHIELD_INCLUDE_COMMON__H___
#define CNC_SHIELD_INCLUDE_COMMON__H___

enum {
  STEPPER_SET_TARGET_POS = 0,
  STEPPER_GET_CURRENT_POS = 1,
  STEPPER_SET_STEP_DELAY_MICRO_SECS = 2,
  STEPPER_START_HOMING = 3,
  STEPPER_GET_HOMING_STATUS = 4,
};

const byte MAX_REC_BYTES = 3;
const byte MAX_STEPS_PER_LOOP = 10;
const uint16_t DEFAULT_STEP_DELAY_MICRO_SECS = 4000;

struct PinInfo {
  byte dir;
  byte step;
  byte endstop;
};

struct StepperInfo {
  PinInfo pin;
  uint16_t pos;
  int16_t pos_target_offset;
  uint16_t step_delay_micro_secs;
  bool is_dir_plus;
} steppers[] = {
  {{5,2,0},0,0,DEFAULT_STEP_DELAY_MICRO_SECS, false},
  {{6,3,0},0,0,DEFAULT_STEP_DELAY_MICRO_SECS, false},
  {{7,4,0},0,0,DEFAULT_STEP_DELAY_MICRO_SECS, false},
};

// Homing info for the current stepper that is trying to home.
struct HomingInfo {
  bool is_pending = false;
  byte stepper;
  bool is_dir_plus = true;
  byte slowness = 4;
} homing_info;

byte num_steppers() {
  return sizeof(steppers)/sizeof(steppers[0]);
}

void _setStepperDir(StepperInfo *stepper, bool is_dir_plus) {
  if (is_dir_plus != stepper->is_dir_plus) {
    digitalWrite(stepper->pin.dir, is_dir_plus ? HIGH : LOW);
    stepper->is_dir_plus = is_dir_plus;
  }
}

void setupBoard() {
  for(byte i=0; i<num_steppers(); i++) {
    pinMode(steppers[i].pin.dir, OUTPUT);
    pinMode(steppers[i].pin.step, OUTPUT);
    digitalWrite(steppers[i].pin.dir, LOW);
    digitalWrite(steppers[i].pin.step, LOW);
  }
}

void runOneStep(byte pin, int delay) {
    digitalWrite(pin, HIGH);
    delayMicroseconds(delay);
    digitalWrite(pin, LOW);
    delayMicroseconds(delay);
}

void runStepper(StepperInfo *stepper) {
  Serial.print("run");
  Serial.println(millis());
  bool is_dir_plus = stepper->pos_target_offset>0;
  int delay = stepper->step_delay_micro_secs;
  _setStepperDir(stepper, is_dir_plus);
  for(byte i=0; i<MAX_STEPS_PER_LOOP; i++) {
    if (stepper->pos_target_offset==0) return;
    stepper->pos_target_offset -= is_dir_plus ? 1 : -1;
    stepper->pos += is_dir_plus ? 1 : -1;
    runOneStep(stepper->pin.step, delay);
  }
}


void tryHoming() {
  if (!homing_info.is_pending) return;
  if (homing_info.stepper >= num_steppers()) return;
  auto &stepper = steppers[homing_info.stepper];
  int delay = stepper.step_delay_micro_secs*homing_info.slowness;
  _setStepperDir(&stepper, homing_info.is_dir_plus);
  for(byte i=0; i<MAX_STEPS_PER_LOOP; i++) {
    if (digitalRead(stepper.pin.endstop)) {
      // Endstop is pressed. Now, reverse a bit.
      _setStepperDir(&stepper, !homing_info.is_dir_plus);
      for(byte j=0; j<100; j++) {
        runOneStep(stepper.pin.step, delay*2);
      }
      // Slowly move forward until endstop is pressed again.
      _setStepperDir(&stepper, homing_info.is_dir_plus);
      for(byte j=0; j<110; j++) {
        if (digitalRead(stepper.pin.endstop)) {
          // We hit the endstop again. This is the final homing position.
          homing_info.is_pending = false;
          homing_info.stepper = 0xFF;
          return;
        }
        runOneStep(stepper.pin.step, delay*2);
      }
      return;
    }
    runOneStep(stepper.pin.step, delay);
  }
}

byte handleRequest(byte cmd, byte* buf, byte buf_len) {
  assert(buf_len == MAX_REC_BYTES);
  byte cmd_extra = cmd & 0xF;
  switch((cmd & 0xF0)>>4) {
    case STEPPER_GET_CURRENT_POS: {
      Serial.println("get");
      Serial.println(cmd_extra);
      Serial.println(steppers[cmd_extra].pos);
      buf[0] = 0xFF;
      buf[1] = 0xFF;
      if (cmd_extra < num_steppers()) {
        buf[0] = (steppers[cmd_extra].pos & 0xFF00) >> 8;
        buf[1] = steppers[cmd_extra].pos & 0xFF;
      }
      return 2;
    }
    case STEPPER_GET_HOMING_STATUS: {
      byte buf[2];
      buf[0] = homing_info.is_pending;
      buf[1] = homing_info.stepper;
      return 2;
    }
  }
  return 0;
}

bool handleMsgReceive(byte *buf, byte buf_len) {
  assert(buf_len == MAX_REC_BYTES);
  byte cmd = (buf[0] & 0xF0)>>4;
  byte cmd_extra = buf[0] & 0xF;
  Serial.println("msg");
  Serial.println(cmd);
  Serial.println(cmd_extra);
  switch (cmd) {
    case STEPPER_SET_TARGET_POS:
      if (cmd_extra < num_steppers() && buf_len == MAX_REC_BYTES) {
        Serial.println("set");
        uint16_t target = (buf[1]<<8) + buf[2];
        Serial.println(target);
        Serial.println(buf[1], HEX);
        Serial.println(buf[2], HEX);
        steppers[cmd_extra].pos_target_offset = target - steppers[cmd_extra].pos;
      }
      return true;
    case STEPPER_SET_STEP_DELAY_MICRO_SECS:
      if (cmd_extra < num_steppers() && buf_len == MAX_REC_BYTES) {
        steppers[cmd_extra].step_delay_micro_secs = (buf[1]<<8) + buf[2];
      }
      return true;
    case STEPPER_START_HOMING:
      if (cmd_extra < num_steppers() && buf_len == MAX_REC_BYTES) {
        homing_info.stepper = cmd_extra;
        homing_info.is_dir_plus = buf[1];
        homing_info.slowness = buf[2];
        homing_info.is_pending = true;
      }
  }
  return false;
}
#endif // CNC_SHIELD_INCLUDE_COMMON__H___
