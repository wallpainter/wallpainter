import smbus
import time

STEPPER_SET_TARGET_POS = 0
STEPPER_GET_CURRENT_POS = 1
STEPPER_SET_STEP_DELAY_MICRO_SECS = 2
STEPPER_START_HOMING = 3
STEPPER_GET_HOMING_STATUS = 4


def _cmd(cmd, stepper):
    return ((cmd & 0xF) << 4) + (stepper & 0xF)


class CNCShieldI2C:
    def __init__(self, slave):
        self._bus = smbus.SMBus(1)
        self._slave = slave

    def set_target_pos(self, stepper, pos):
        data = ((pos & 0xFF00) >> 8, pos & 0xFF)
        self._bus.write_i2c_block_data(self._slave,
                                       _cmd(STEPPER_SET_TARGET_POS, stepper),
                                       data)

    def set_step_delay(self, stepper, delay):
        data = ((delay & 0xFF00) >> 8, delay & 0xFF)
        self._bus.write_i2c_block_data(
            self._slave, _cmd(STEPPER_SET_STEP_DELAY_MICRO_SECS, stepper),
            data)

    def current_pos(self, stepper):
        data = self._bus.read_i2c_block_data(
            self._slave, _cmd(STEPPER_GET_CURRENT_POS, stepper), 2)
        return (data[0] << 8) + data[1]

    def _get_homing_status(self, stepper):
        data = self._bus.read_i2c_block_data(
            self._slave, _cmd(STEPPER_GET_HOMING_STATUS, stepper), 2)
        if data[0] == 0:
            return None
        return data[1]

    def do_homing(self, stepper, direction, slowness=2):
        print('Initiate homing for ', stepper)
        data = (1 if direction else 0, slowness & 0xFF)
        self._bus.write_i2c_block_data(self._slave,
                                       _cmd(STEPPER_START_HOMING, stepper),
                                       data)
        while True:
            homing_stepper = self._get_homing_status()
            if homing_stepper != None:
                print('Homing in progress for ', homing_stepper)
                time.sleep(1)


if __name__ == "__main__":
    s = CNCShieldI2C(4)
    print(s.current_pos(0))
    print(s.current_pos(1))
    print(s.current_pos(2))
    print(s.current_pos(3))
